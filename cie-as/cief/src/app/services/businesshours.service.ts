import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface myData {
  Anno: string,
  IDAlumno: string,
  Nombre: string,
  FechaIni: string,
  Tipo: string,
  IDGrupo: string,
  NomGrupo: string,
  PesoActivo: number,
  PesoPasivo: number,
  TParticp: string,
  Horas: number,
  FotoRuta: string,
  DependenciaEvento: string,
  IDDependencia: string,
  NomEscuela: string,
  Nivel: string,
  Certificado: string,
  Premiado: string,
  Observacion: string
}

@Injectable({
  providedIn: 'root'
})
export class BusinesshoursService {

  constructor(private http:HttpClient) { }

  getBusinessHours(idalumno):Observable<any>{
     return this.http.get('http://localhost:3801/api/businesshours/'+idalumno);
  }
}
