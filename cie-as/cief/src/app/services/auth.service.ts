
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

interface myData {
   success: boolean,
   message: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');
  
  constructor(private http:HttpClient) { }

  setLoggedIn(value:boolean){
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn','true');     
    //console.log(localStorage);
  }
  
  get isLoggedIn(){
    //return this.loggedInStatus;
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString());
  }

  getUserDetalis(username,password):Observable<any>{
    //post these details to API server return user info if correct
    return this.http.post<myData>('http://localhost:3801/api/user/',{idUser:username,userPwd:password});
    //.pipe(map( (res:Response) => res.json() as any));    
    
  }
}
