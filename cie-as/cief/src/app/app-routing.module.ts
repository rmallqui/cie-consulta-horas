
/** Módulos de enrutado de Angular2 */
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

// Array con las rutas de este módulo. Ninguna funcional.
const routes: Routes = [
 // { path:'login',component:LoginComponent },  
 // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'dashboard', component:DashboardComponent }//,
  //{ path: '**', redirectTo: 'login', pathMatch: 'full' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes) // configuración para el módulo raíz
  ],
  exports: [
    RouterModule // se importará desde el módulo padre
  ]
})
export class AppRoutingModule { }