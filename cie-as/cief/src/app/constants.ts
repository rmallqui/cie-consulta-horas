import { environment } from '../environments/environment.prod';
export const URI:string = (environment != null ) ? environment.uri : 'http://localhost:3801/api';