import { BusinesshoursService } from './../../services/businesshours.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

interface myData {
  success: boolean,
  message: string
}

@Component({
  selector: 'app-businesshours',
  templateUrl: './businesshours.component.html',
  styleUrls: ['./businesshours.component.css']
})
export class BusinesshoursComponent implements OnInit {
  idalumno:string;
  hours:any[];
  vData:boolean = false;
  vNdata:boolean = false;
  constructor(public businesshoursService:BusinesshoursService) {
    this.hours = [{message:"No data"}];
    console.log(this.hours);
   }

  ngOnInit() {
    this.hours = [{message:"No data"}];
  }

  geDatatHours(){
    this.businesshoursService.getBusinessHours(this.idalumno).subscribe(response => {
        this.hours = [];
        if (response.data == undefined) {
          this.hours = [{message:"No data"}];
        }

        if (response.message != undefined)
        {
          this.hours = [{message:"No data"}];;   
        } 
        else 
        {
          this.hours = response.data;          
        } 
        this.viewData();       
        console.log(this.hours);
       // console.log(response);
    });
  }

  viewData(){
     if (this.hours[0].message != undefined) {        
        this.vNdata = true;
        this.vData = false;
       
     }
     else //if (this.hours[0] != "No data")
     {
        this.vData = true;
        this.vNdata = false;
     } 
     console.log('vNData:'+this.vNdata+'- vData:'+this.vData);   
  }

}
