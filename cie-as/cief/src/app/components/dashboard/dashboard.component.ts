import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  username:string;
  timeString : string;
  // duration = 10*60;
  duration = 1;
  seconds = "";
  minutes = "";   
  clockDisplay : string; 
  interval: number;
  constructor(
    private auth:AuthService,
    private router:Router
  ) {
    this.username = localStorage.getItem("username");
   }

  ngOnInit() {
    //console.log(this.minutes + " : " + this.seconds);
    this.tickTick();
    console.log(localStorage);
  }
  
  tickTick(){
    //event.preventDefault();
    if(this.duration > 0)
    {
        setInterval( () => 
        {          
          this.duration = this.duration + 1;
         
          if(this.duration <= 0 )
          {
              clearInterval(this.interval)	
          }

          this.seconds = (parseInt((this.duration%60).toString())).toString();
          this.minutes = (parseInt((this.duration/60).toString())).toString();
          
          this.clockDisplay = this.minutes + ":" +this.seconds; 
          
          localStorage.setItem("seconds",this.seconds);

          if (localStorage.getItem("seconds") == '600')
            {
              this.loggedOut();              
            }
        }
        ,1000); 
    }
  }

  loggedOut(){
        event.preventDefault();
        this.auth.setLoggedIn(false);
        localStorage.clear();
        this.router.navigate(['login']);
  }
}
